//
//  ViewController.m
//  LocationLier
//
//  Created by valentine.wang on 2018/6/29.
//  Copyright © 2018年 valentine.wang. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <WebKit/WebKit.h>
/*
 http://www.gpsspg.com/maps.htm采用谷歌地球地图的经纬度
 */

@interface ViewController () <CLLocationManagerDelegate,WKUIDelegate,WKNavigationDelegate>
{
    WKWebView *MapView;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //    <!--在倒数三位内调整>
    CLLocationManager *manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status == kCLAuthorizationStatusNotDetermined) {
        [manager requestWhenInUseAuthorization];
        [manager requestLocation];
        [manager requestAlwaysAuthorization];
    }

    MapView = [[WKWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    MapView.UIDelegate = self;
    MapView.navigationDelegate = self;
    [self.view addSubview:MapView];
    [MapView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://map.baidu.com/mobile/webapp/index/index/foo=bar/vt=map/?fromhash=1"]]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(4, 150, 500, 20)];
    label.text = [NSString stringWithFormat:@"%f  %f ",manager.location.coordinate.latitude,manager.location.coordinate.longitude];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.view addSubview:label];
    });
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations{

}

- (void)locationManager:(CLLocationManager *)manager
       didUpdateHeading:(CLHeading *)newHeading{
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    
}


- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler {
    completionHandler(NSURLSessionAuthChallengePerformDefaultHandling,nil);
}



@end
